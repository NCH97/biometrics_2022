/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */



import React,{ useState } from 'react';
import type {Node} from 'react';
import { AsyncStorage } from 'react-native';
import ReactNativeBiometrics from 'react-native-biometrics';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  TextInput,
  Button,
} from 'react-native';

import {
  Colors,
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

const App: () => Node = () => {
  const isDarkMode = useColorScheme() === 'dark';

  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,

  };

  const [login_nom, onChangeLoginlastName] = React.useState(null);
  const [login_prenom, onChangeLoginfirstName] = React.useState(null);

  const [register_nom, onChangeRegisterlastName] = React.useState(null);
  const [register_prenom, onChangeRegisterfirstName] = React.useState(null);

  async function storeUser(nom,prenom){
    let user = {
        lastname : nom,
        firstname: prenom
    }

     try {
           await AsyncStorage.setItem('user',JSON.stringify(user))
            alert('welcome ' + user.lastname + ' ' + user.firstname)
        } catch (error) {
          console.log("Something went wrong", error);
        }
  }

  async function getUser(){
       let user;
       try {
             user = await AsyncStorage.getItem("user");
             let username = JSON.parse(user);
             alert(username.firstname + ' ' + username.lastname);
             return user;
          } catch (error) {
            console.log("Something went wrong", error);
          }
    }

    const AuthBiometric=async()=>{
      try {
        const { success } =  await ReactNativeBiometrics.simplePrompt({promptMessage: 'Confirm fingerprint'})
        if (success) {
          return true;
        } else {
          return false;
        }
       } catch (error) {
        console.log('biometrics failed',error)
        return false;
      }
    }

    async function registerwithBiometric(nom,prenom) {
        storeUser(nom,prenom)
        let epochTimeSeconds = Math.round((new Date()).getTime() / 1000).toString()
        let payload = epochTimeSeconds + 'some message'

        ReactNativeBiometrics.createSignature({
            promptMessage: 'Sign in',
            payload: payload
          })
          .then((resultObject) => {
            const { success, signature } = resultObject

            if (success) {
              console.log(signature)
              verifySignatureWithServer(signature, payload)
            }
          })
      }

    async function loginBiometrics () {
        let user;
       try {
             user = await AsyncStorage.getItem("user");
             let username = JSON.parse(user);
             if (user){

                         let sign= await AuthBiometric();

                         if(sign === true){
                             alert(username.firstname + ' ' + username.lastname + " You are Connected")
                         }
                     }
          } catch (error) {
            console.log("Something went wrong", error);
          }


    }




  return (
    <SafeAreaView style={backgroundStyle}>
      <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
      <ScrollView
        contentInsetAdjustmentBehavior="automatic"
        style={backgroundStyle}>
        <Header />
        <View
          style={{
            backgroundColor: isDarkMode ? Colors.black : Colors.white,
          }}>
          <Text title="Login">Login</Text>
            <TextInput
                      style={{height: 40}}
                      onChangeText={onChangeLoginlastName}
                      value={login_nom}
              />
            <Button
              onPress={() => {
                loginBiometrics ()
              }}
              title="Se Connecter"
            />
          <Text title="Register">REGISTER</Text>
              <TextInput
                      style={{height: 40}}
                      onChangeText={onChangeRegisterfirstName}
                      value={register_prenom}
                      placeholder="Prenom"

              />
               <TextInput
                        style={{height: 40}}
                        onChangeText={onChangeRegisterlastName}
                        value={register_nom}
                        placeholder="Nom"
                />

          <Button
            onPress={() => {

              registerwithBiometric(register_nom,register_prenom);

            }}
            title="S'inscrire"
          />


        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
  },
  highlight: {
    fontWeight: '700',
  },
});

export default App;
